#!/usr/bin/python3.8
# -*- coding: utf-8 -*-
# Copyright (c) 2021 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import sys

from bundle_check.bundle_json_check import BundleCheckTools
from gn_check.check_gn_online import CheckGnOnline
from guard_stuff.csct_with_giteePR import GiteeCsctPrehandler
from prettytable import PrettyTable


def main():
    if len(sys.argv) == 1:
        sys.stderr.write('False')
        return

    repo_prNum_config_path = sys.argv[1]
    csct_prehandler = GiteeCsctPrehandler(repo_prNum_config_path, "BUILD.gn", "bundle.json", ".gni")

    gn_status, gn_errs = CheckGnOnline(csct_prehandler.get_diff_dict("BUILD.gn")).output()
    gni_status, gni_errs = CheckGnOnline(csct_prehandler.get_diff_dict(".gni")).output()
    bundle_status, bundle_errs = BundleCheckTools.check_diff(csct_prehandler.get_diff_dict("bundle.json"))
    errs_info = bundle_errs + gn_errs + gni_errs

    if len(errs_info)==0:
        sys.stderr.write("True")
        return

    table = PrettyTable(['文件', '定位', '违反规则', '错误说明'])
    table.add_rows(errs_info)
    table.align['文件'] = 'l'
    table.align['定位'] = 'l'
    table.align['错误说明'] = 'l'
    info = table.get_string()
    print('There are the following {} questions'.format(len(errs_info)))
    

    sys.stdout.write(str(info))
    sys.stderr.write(str(gn_status))
    return


if __name__ == '__main__':
    sys.exit(main())
