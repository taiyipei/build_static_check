#!/usr/bin/python3.8
# -*- coding: utf-8 -*-
# Copyright (c) 2021 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import sys
import subprocess

def csct_online(repo_prNum_config):
    status, result = True, ""
    root_dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    csct_project_path = os.path.join(root_dir, "build_static_check/tools/component_tools/static_check/")
    repo_prNum_config_path = os.path.abspath(repo_prNum_config)

    try:
        cmd = "python3 " + csct_project_path + "csct_online.py " + repo_prNum_config_path
        print(cmd)
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE, errors='replace')
        result, errcode = p.communicate()
        status = True if errcode == 'False' else False
    except Exception as r:
        print('error %s' %(r), cmd)

    return status, result

if __name__ == '__main__':
    sys.exit(csct_online("./repo_prNum.conf"))
