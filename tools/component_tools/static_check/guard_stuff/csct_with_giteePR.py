#!/usr/bin/python3.8
# -*- coding: utf-8 -*-
# Copyright (c) 2021 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import sys
import re
import argparse
import subprocess
import chardet

file_dir=os.path.dirname(os.path.abspath(__file__))

def add2dict(diff_dict, path, line_num: str, content):
    key = path
    value = [line_num, content]

    if key in diff_dict:
        value_list = diff_dict.pop(key)
        value_list.append(value)
    else:
        value_list = [value]

    diff_dict.update({key:value_list})

def strip_diff(diff_dict, gitee_path_prefix, gitee_pr_diff):
    pattern1 = "---\ (a/)?.*"
    pattern2 = "\+\+\+\ b/(.*)"
    pattern3 = "@@\ -[0-9]+,[0-9]+\ \+([0-9]+)(,[0-9]+)?\ @@.*"
    pattern4 = "[\ +-](.*)"
    pattern5 = "([\ +])?(.*)"
    line_num = 0
    fullpath = ""
    match_flag = False
    curr_key = ""

    for line in gitee_pr_diff.splitlines():
        if re.match(pattern1, line) is not None:
            match_flag = False
            continue
        elif re.match(pattern2, line) is not None:
            for key in diff_dict:
                if re.search(key, line) is not None:
                    curr_key = key
                    res = re.match(pattern2, line)
                    fullpath = gitee_path_prefix + res.group(1).strip()
                    match_flag = True
        elif match_flag == True and re.match(pattern3, line) is not None:
            res =re.match(pattern3, line)
            line_num=int(res.group(1))
        elif match_flag == True and re.match(pattern4, line) is not None:
            res = re.match(pattern5, line)
            if res.group(1) == '+':
                add2dict(diff_dict[curr_key], fullpath, line_num, res.group(2))
            if res.group(1) != '-':
                line_num=line_num+1

def get_diff_by_repo_prNum(repo_url, prNum):
    diff_url=repo_url+"/pulls/"+str(prNum)+".diff"
    cmd="curl -s "+diff_url
    gitee_pr_diff = ""
    try:
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE, errors='replace')
        gitee_pr_diff, errorout = p.communicate()
    except Exception as r:
        print('error %s' %(r), cmd)
        with open("fail.log", "a") as f:
            f.write(cmd+'\n')

    return gitee_pr_diff

class GiteeCsctPrehandler:
    def __init__(self, repo_num_file: str, *patterns):
        '''
        'diff_dict' struct organized as:
            self.diff_dict = {
                 "BUILD.gn": {},
                 "bundle.json":{},
                 ".gni":{}
            }

         TODO:diff_dict is considered to be more flexible, allowing 
         registering.
        '''
        self.diff_dict = {}

        for pattern in patterns:
           pattern_dict = {pattern: {}}
           self.diff_dict.update(pattern_dict)

        try:
            f = open(repo_num_file, 'r')
            repo_prNum_list = f.read().split('\n')
        except Exception as r:
            print("error %s" %(r))
        else:
            for pr_item in repo_prNum_list[:-1]:
                repo_num_temp=pr_item.split(',')
                repo_url = repo_num_temp[0]
                prNum_temp = repo_num_temp[1].split('-')
                prNum_start = int(prNum_temp[0])
                prNum_end = prNum_start + 1
                if len(prNum_temp) > 1:
                    prNum_end = int(prNum_temp[1]) + 1
                for pn in range(prNum_start, prNum_end):
                    gitee_pr_diff = get_diff_by_repo_prNum(repo_url, pn)
                    strip_diff(self.diff_dict, repo_url, gitee_pr_diff)

            f.close()

    def clear_repo_num_file(self):
        self.diff_dict.clear()

    def get_diff_dict(self, pattern):
        return self.diff_dict[pattern]

def test(repo_num_file):
    result = "test finish"
    csct_prehandler = GiteeCsctPrehandler(repo_num_file,
            "BUILD.gn", "bundle.json", ".gni")

    print("==================start get diff====================")
    print(csct_prehandler.get_diff_dict("BUILD.gn"))
    print("==================start get diff====================")
    print(csct_prehandler.get_diff_dict("bundle.json"))
    print("========================end=========================")
    return True, result

if __name__ == '__main__':
    sys.exit(test("repo_prNum.conf"))
