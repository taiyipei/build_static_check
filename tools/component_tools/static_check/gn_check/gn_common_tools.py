import logging
import os
import re
import typing


class GnCommon:
    """
    处理BUILD.gn文件的通用方法
    """

    @staticmethod
    def contains_keywords(key_words: tuple, target: str) -> bool:
        """
        判断target中是否包含key_words中的元素
        """
        for k in key_words:
            if k in target:
                return True
        return False

    @staticmethod
    def find_files(project_path: str,
                   target_filename: list = ["BUILD.gn", "*.gni"],
                   black_keywords: tuple = tuple(),
                   black_dirs: tuple = tuple()) -> set:
        """
        基于linux的find命令查找文件
        """
        cmd = "find {}".format(project_path)
        cmd += " -name {} -or -name {}".format(target_filename[0],target_filename[1])
        result_set = set()
        for bd in black_dirs:
            bd_path = os.path.join(project_path, bd)
            cmd += " -o -path '{}' -prune".format(bd_path)
        output = os.popen(cmd)
        for file in output:
            if GnCommon.contains_keywords(black_keywords, file):
                continue
            result_set.add(file.strip())
        logging.info("total: {}".format(len(result_set)))
        return result_set

    @staticmethod
    def grep_one(grep_pattern: str,
                 grep_path: str,
                 exclude: tuple = tuple(),
                 black_keyword: tuple = tuple(),
                 include=["BUILD.gn","*.gni"],
                 grep_parameter='Ern'):
        """
        调用linux的grep命令，开启了通用的正则匹配
        grep_path：可以是路径，也可以是文件
        没有grep到内容，返回None
        """
        cmd = "grep -{} '{}' {}".format(grep_parameter,
                                        grep_pattern, grep_path)
        for f in include:
            cmd += " --include={}".format(f)
        for e in exclude:
            cmd += " --exclude-dir={}".format(e)
        if len(black_keyword) != 0:
            cmd += " | grep -Ev '{}'".format("|".join(black_keyword))
        # cmd += " | head -n 1"
        logging.info(cmd)
        result = None
        output = os.popen(cmd).read().strip()
        if len(output) != 0:
            result = output
        return result

    @staticmethod
    def find_paragraph_iter(pattern: str, content: str) -> typing.Iterator:
        """
        匹配所有pattern
        #    "ohos_shared_library": r"^( *)ohos_shared_library\(\".*?\"\)[\s|\S]*?\n\1\}$",
        pattern: 要
        example: 匹配ohos_shared_library
        iter = GnCommon.find_paragraph_iter(start_pattern="ohos_shared_library", end_pattern="\}", content=filecontent)
        for i in iter:
            print(i.group())
        caution：如果是嵌套内容，则只能匹配外层的
        """
        ptrn = re.compile(pattern, flags=re.S | re.M)
        result = re.finditer(ptrn, content)
        return result
