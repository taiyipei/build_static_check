import re
import sys

sys.path.append('..')
from guard_stuff import csct_with_giteePR



class CheckGnOnline(object):

    def __init__(self, gn_data: dict) -> None:
        self.STATUS = True
        self.gn_data = gn_data
        self.err_info = list()

    def merge_line(self) -> dict:
        ret_dict = dict()
        for key, values in self.gn_data.items():
            contents = ''
            row_range = list()
            start = -2
            end = -2
            for line in values:
                pos = line[0]
                content = line[1]

                if contents == '':
                    contents = content
                    start = pos
                    end = pos
                    continue

                if pos == end+1:
                    end = pos
                    contents += '\n{}'.format(content)
                elif start != end:
                    row_range.append([start, end, contents])
                    contents = ''
                    start = pos
                    end = pos
                elif start == end:
                    contents = content
                    start = pos
                    end = pos

                if pos == values[-1][0] and start != end:
                    row_range.append([start, end, contents])

            ret_dict.update({key: row_range})
        return ret_dict

    def check_have_product_name(self, key: str, line: list) -> None:
        RULES = '规则4.1 部件编译脚本中禁止使用产品名称变量'
        CHECK_LIST = ['product_name', 'device_name']
        flag = {CHECK_LIST[0]: False, CHECK_LIST[1]: False}
        for check_item in CHECK_LIST:
            if line[1].find(check_item) != -1:
                flag[check_item] = True
        if any(flag.values()):
            issue = '存在'
            issue += CHECK_LIST[0] if flag[CHECK_LIST[0]] else ''
            issue += ',' if all(flag.values()) else ''
            issue += CHECK_LIST[1] if flag[CHECK_LIST[1]] else ''
            pos = "line:{}  {}".format(line[0], line[1])
            self.err_info.append([key, pos, RULES, issue])
            self.STATUS = False
        return

    def check_abs_path(self, key: str, line: list) -> None:
        RULES = '规则3.1 部件编译脚本中只允许引用本部件路径，禁止引用其他部件的绝对或相对路径'
        ISSUE = '存在绝对路径'
        ABS_PATH_PATTERN = r'"\/{0,1}(\/[^\/\n]*)+"'
        abs_info = re.findall(ABS_PATH_PATTERN, line[1])
        if len(abs_info) > 0:
            pos = "line:{}  {}".format(line[0], line[1])
            self.err_info.append([key, pos, RULES, ISSUE])
            self.STATUS = False
        return

    def check_pn_sn(self) -> None:
        RULES = '规则3.2 部件编译目标必须指定部件和子系统名'
        TARGET_PATTERN = r"^( *)(ohos_shared_library|ohos_static_library|ohos_executable_library|ohos_source_set)[\s|\S]*?\n\1}$"
        CHECK_LIST = ['subsystem_name', 'part_name']
        gn_data_merge = self.merge_line()
        for key, values in gn_data_merge.items():
            for line in values:
                targets = re.finditer(TARGET_PATTERN, line[2], re.M)
                for it in targets:
                    flag = {CHECK_LIST[0]: False, CHECK_LIST[1]: False}
                    target = it.group()
                    if target.find(CHECK_LIST[0]) == -1:
                        flag[CHECK_LIST[0]] = True
                    if target.find(CHECK_LIST[1]) == -1:
                        flag[CHECK_LIST[1]] = True
                    if any(flag.values()):
                        issue = 'target不存在'
                        issue += CHECK_LIST[0] if flag[CHECK_LIST[0]] else ''
                        issue += ',' if all(flag.values()) else ''
                        issue += CHECK_LIST[1] if flag[CHECK_LIST[1]] else ''
                        pos = "line:{}-{}  {}".format(
                            line[0], line[1], line[2])
                        self.err_info.append([key, pos, RULES, issue])
                        self.STATUS = False

        return

    def check(self):
        if not self.gn_data:
            return
        for key, values in self.gn_data.items():
            for line in values:
                self.check_have_product_name(key, line)
                self.check_abs_path(key, line)
        self.check_pn_sn()

    def output(self):
        self.check()
        
        return self.STATUS,self.err_info

    
if __name__ == "__main__":
    data = {'a.gn': [[11, 'dsfsdf product_name'], [13, '("//build/dasd/")']],
            'b.gn': [[5, 'ohos_shared_library("librawfile") {'],
                     [15, 'ohos_shared_library("librawfile") {'],
                     [16, '  include_dirs = ['],
                     [17, '    "//base/global/resource_management/frameworks/resmgr/include",'],
                     [18, '  subsystem_name = "global"'], [19, '}'],
                     [25, '  subsystem_name = "global"'], [29, '}']]}
    data1 = csct_with_giteePR.run('../guard_stuff/repo_prNum.conf')
    a = CheckGnOnline(data1)
    a.output()
    exit(0)
