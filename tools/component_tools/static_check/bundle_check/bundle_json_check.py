#! /usr/bin/python

import os
import json
import argparse
import re
from posixpath import isabs
import pandas as pd
from prettytable import PrettyTable

CHECK_RULE_2_1 = r"规则2.1 部件描述文件中字段须准确。"

NAME_NO_FIELD = '缺少 "name" 字段。'
NAME_EMPTY = '"name" 字段不能为空。'
NAME_FORMAT_ERROR = r"格式错误, 命名规则为: @{organization}/{component_name}。"
NAME_LOWCASE = r'"name" 字段的英文字符均为小写。'
COMPONENT_NAME_FROMAT = r"部件名使用小写加下划线风格命名，如：unix_like。"
COMPONENT_NAME_FROMAT_LEN = r"部件名不能超过 63 个有效英文字符."

VERSION_NO_FIELD = '缺少 "version" 字段。'
VERSION_ERROR = r'"version" 有误，请检查。'

SEGMENT_NO_FIELD = r'缺少 "segment" 字段。'
SEGMENT_DESTPATH_NO_FIELD = r'缺少 "segment:destPath" 字段。'
SEGMENT_DESTPATH_EMPTY = r'"segment:destPath" 字段不能为空。'
SEGMENT_DESTPATH_UNIQUE = r'"segment:destPath" 只能有一个路径。'
SEGMENT_DESTPATH_ABS = r'"segment:destPath" 只能为相对路径。'

COMPONENT_NO_FIELD = r'缺少 "component" 字段。'
COMPONENT_NAME_NO_FIELD = '缺少 "component:name" 字段。'
COMPONENT_NAME_EMPTY = r'"component:name" 字段不能为空。'
COMPONENT_NAME_VERACITY = r'"component:name" 字段应该与 "name" 字段部件名部分相同。'
COMPONENT_SUBSYSTEM_NO_FIELD = r'缺少 "component:subsystem" 字段。'
COMPONENT_SUBSYSTEM_EMPTY = r'"component:subsystem" 字段不能为空。'
COMPONENT_SUBSYSTEM_LOWCASE = r'"component:subsystem" 值必须为全小写字母。'
# COMPONENT_SYSCAP_NO_FIELD = r'缺少 "component:syscap" 字段。'
COMPONENT_SYSCAP_STRING_EMPTY = r'"component:syscap" 中的值不能为空字符串。'
COMPONENT_SYSCAP_HEADER_ERROR = r'"component:syscap" 中的值必须以 "SystemCapability." 开头。'
COMPONENT_SYSCAP_STRING_STYLE = r'"component:syscap" 中的值必须为大驼峰命名规则。'
COMPONENT_SYSCAP_STRING_FORMAT_ERROR = r'"component:syscap" 中的值命名规则为 "SystemCapability.子系统.部件能力.子能力（可选）"。\
    如，SystemCapability.Media>Camera，SystemCapability.Media.Camera.Front。'
# COMPONENT_FEATURES_NO_FIELD = r'缺少 "component:features" 字段。'
COMPONENT_FEATURES_STRING_EMPTY = r'"component:features" 中的值不能为空字符串。'
COMPONENT_FEATURES_SEPARATOR_ERROR = r'"component:features" 的值须加上部件名为前缀并以 "_" 为分隔符"。'
COMPONENT_AST_NO_FIELD = r'缺少 "component:adapted_system_type" 字段。'
COMPONENT_AST_EMPTY = r'"component:adapted_system_type" 字段不能为空。'
COMPONENT_AST_NO_REP = r'"component:adapted_system_type" 值最多只能有 3 个（"mini", "small", "standard"）且不能重复。'
COMPONENT_ROM_NO_FIELD = r'缺少 "component:rom" 字段。'
COMPONENT_ROM_EMPTY = r'"component:rom" 字段不能为空。'
COMPONENT_ROM_SIZE_ERROR = r'"component:rom" 非数值或者小于等于0。'
COMPONENT_ROM_UNIT_ERROR = r'"component:rom" 的单位错误（KB, KByte, MByte, MB，默认为KByte）'
COMPONENT_RAM_NO_FIELD = r'缺少 "component:ram" 字段。'
COMPONENT_RAM_EMPTY = r'"component:ram" 字段不能为空。'
COMPONENT_RAM_SIZE_ERROR = r'"component:ram" 非数值或者小于等于0。'
COMPONENT_RAM_UNIT_ERROR = r'"component:ram" 的单位错误（KB, KByte, MByte, MB，默认为KByte）'
COMPONENT_DEPS_NO_FIELD = r'缺少 "component:deps" 字段。'

class BundleCheckTools:
    '''用于检查 bundle.json 的工具箱。'''
    @staticmethod
    def is_all_lower(s:str):
        '''
        @func: 判断字符串是否有且仅有小写字母。
        '''
        if not s:
            return False
        for i in s:
            if not (97 <= ord(i) <= 122):
                return False
        return True

    @staticmethod
    def is_number(s:str) -> bool:
        '''
        @func: 判断字符串 s 是否能表示为数值。
        '''
        try:
            float(s)
            return True
        except:
            return False

    @staticmethod
    def split_by_unit(s:str):
        '''
        @func: 分离 s 字符串的数值和单位
        '''
        if s[0] == '~': # 去除 ram 前面的 '~' 字符
            s = s[1:]
        if BundleCheckTools.is_number(s):
            return float(s), ''
        for n, u in enumerate(s):
            if not (u.isdigit() or u == '.' or u == '-'):
                break
        try:
            num = float(s[:n])
        except ValueError as e:
            num = 0
        unit = s[n:]
        return num, unit

    @staticmethod
    def is_project(path:str) -> bool:
        '''
        @func: 判断路径是否源码工程。
        @note: 通过是否存在 .repo/manifests 目录判断。
        '''
        p = os.path.normpath(path)
        return os.path.exists(p + '/' + '.repo/manifests')

    @staticmethod
    def get_all_bundle_json(project_path:str = '.') -> list:
        '''
        @func: 获取所有源码工程中所有 bundle.json 文件。
        '''
        exclude_list = [
            r'"./out/*"',
            r'"./.repo/*"'
        ]
        cmd = "find {} -name {}".format(project_path, "bundle.json")
        for i in exclude_list:
            cmd += " ! -path {}".format(i)
        bundle_josn_list = os.popen(cmd).readlines()
        return bundle_josn_list

    @staticmethod
    def export_to_json(all_errors:dict,
                       output_path:str = '.',
                       output_name:str = 'all_bundle_error.json'):
        '''@func: 导出所有错误到 json 格式文件中。'''
        all_error_json = json.dumps(all_errors,
                                    indent=4,
                                    ensure_ascii=False,
                                    separators=(', ', ': '))
        out_path = os.path.normpath(output_path) + '/' + output_name
        with open(out_path, 'w') as f:
            f.write(all_error_json)
        print("Please check " + out_path)
    
    @staticmethod
    def export_to_excel(all_errors:dict,
                        output_path:str = '.',
                        output_name:str = 'all_bundle_error.xlsx'):
        '''
        @func: 导出所有错误到 excel 格式文件中。
        '''
        df = BundleCheckTools.trans_to_df(all_errors)
        outpath = os.path.normpath(output_path) + '/' + output_name
        df.to_excel(outpath, index=None)
        print('Please check ' + outpath)

    @staticmethod
    def trans_to_df(all_errors:dict) -> pd.DataFrame:
        '''
        @func: 将所有错误的 dict 数据类型转为 pd.DataFrame 类型。
        '''
        columns = ['子系统', '部件', '文件', '违反规则', '详细', '说明']
        errors_list = []
        for subsystem in all_errors:
            for component in all_errors[subsystem]:
                for bundle_path in all_errors[subsystem][component]:
                    for rule in all_errors[subsystem][component][bundle_path]:
                        for error_item in all_errors[subsystem][component][bundle_path][rule]:
                            content = 'line' + str(error_item['line']) + ': ' + error_item['contents']
                            desc = error_item['description']
                            t = list([subsystem, component, bundle_path, rule, content, desc])
                            errors_list.append(t)
        ret = pd.DataFrame(errors_list, columns=columns)
        return ret

    @staticmethod
    def check_diff(diff:dict):
        '''
        @func: 根据 diff 部分对 bundle.json 进行静态检查。
        @parm: 
            ``diff``: 格式化后的字典类型的 diff 文件修改信息。示例:  
                d = {
                    "path1/bundle.json": [
                        [line1, "content1"],  
                        [line2, "content2"]
                    ]
                }
        '''
        d = {}
        for file_path in  diff:
            d[file_path] = []
            diff_list = diff[file_path]
            for i in diff_list:
                ret = BundleCheckTools._check_diff_by_line(i[1])
                if ret:
                    line = list(("line" + str(i[0]) + ": " + i[1], ret))
                    d[file_path].append(line)
        # trans to list
        l = []
        for f in d:
            for i in d[f]:
                row = [f, CHECK_RULE_2_1]
                row.extend([i[0], i[1]])
                l.append(row)
        if l:
            return True, l
        else:
            return False, l

    def _check_diff_by_line(s:str) -> str:
        s = s.strip()
        match = re.match(r'"(\w+)":\s*"(.*)"', s)
        if not match:
            return None
        key = match.group(1)
        value = match.group(2)
        if key == 'name':
            if value.startswith('//') and ':' in value:
                # exclude inner_kits:name
                return None
            v = value.split('/')[1] if ('/' in value) else value
            if not (0 < len(v) < 64):
                return COMPONENT_NAME_FROMAT_LEN
            if not re.match(r'([a-z]+_)*([a-z]+)\b', v):
                return COMPONENT_NAME_FROMAT
            return None
        if key == 'version':
            if len(value) < 3:
                return VERSION_ERROR
            return None
        if key == 'destPath':
            if os.path.isabs(value):
                return SEGMENT_DESTPATH_ABS
            return None
        if key == 'subsystem':
            if not re.match(r'[a-z]+$', value):
                return COMPONENT_SUBSYSTEM_LOWCASE
        if key == 'rom' or key == 'ram':
            if len(value) == 0:
                return r'"component:rom/ram" 字段不能为空。'
            num, unit = BundleCheckTools.split_by_unit(value)
            if num <= 0:
                return '"component:{}" 非数值或者小于等于 0。'.format(key)
            if unit:
                unit_types = ["KB", "KByte", "MByte", "MB"]
                if unit not in unit_types:
                    return '"component:{}" 的单位错误（KB, KByte, MByte, MB，默认为KByte）。'.format(key)
        return None

def check_all_bundle_json(project_path:str) -> dict:
    '''
    @func: 检查指定源码工程下所有 bundle.json 的文件规范。
    '''
    cur_path = os.getcwd()
    os.chdir(project_path)

    all_bundle = BundleCheckTools.get_all_bundle_json()
    all_error = {}
    count = 0
    for bundle_json_path in all_bundle:
        bundle_path = bundle_json_path.strip()
        bundle = BundleJson(bundle_path)
        bundle_error = bundle.check()
        
        subsystem_name = bundle.subsystem_name
        component_name = bundle.component_name
        if len(subsystem_name) == 0:
            subsystem_name = "Unknow"
        if len(component_name) == 0:
            component_name = "Unknow"
        if subsystem_name not in all_error:
            all_error[subsystem_name] = {}
        if component_name not in all_error[subsystem_name]:
            all_error[subsystem_name][component_name] = {}

        if bundle_error:
            component_error = all_error[subsystem_name][component_name]
            component_error[bundle_path] = {}
            component_error[bundle_path][CHECK_RULE_2_1] = bundle_error
            count += len(bundle_error)
    print('-------------------------------')
    print('Bundle.json check successfully!')
    print('There are {} issues in total'.format(count))
    print('-------------------------------')
    os.chdir(cur_path)
    return all_error

def parse_args():
    parser = argparse.ArgumentParser()
    # exclusive output format
    ex_format = parser.add_mutually_exclusive_group()
    ex_format.add_argument("--xlsx", help="output format: xls(default).",
                           action="store_true")
    ex_format.add_argument("--json", help="output format: json.",
                           action="store_true")
    # exclusive input
    ex_input = parser.add_mutually_exclusive_group()
    ex_input.add_argument("-P", "--project", help="project root path.", type=str)
    ex_input.add_argument("-p", "--path", help="bundle.json path list.", nargs='+')
    # output path
    parser.add_argument("-o", "--output", help="ouput path.")
    args = parser.parse_args()

    export_path = '.'
    if args.output:
        export_path = args.output

    if args.project:
        if not BundleCheckTools.is_project(args.project):
            print("'" + args.project + "' is not a oopeharmony project.")
            exit(1)
        
        all_errors = check_all_bundle_json(args.project)
        if args.json:
            BundleCheckTools.export_to_json(all_errors, export_path)
        else:
            BundleCheckTools.export_to_excel(all_errors, export_path)
    elif args.path:
        bundle_list_error = {}
        for bundle_json_path in args.path:
            bundle = BundleJson(bundle_json_path)
            error_field = bundle.check()
            if error_field:
                bundle_list_error[bundle_json_path] = dict([(CHECK_RULE_2_1, error_field)])
        # temp
        test_json = json.dumps(bundle_list_error,
                               indent=4, separators=(', ', ': '), 
                               ensure_ascii=False)
        print(test_json)
    else:
        print("use '-h' get help.")

class BundleJson(object):
    '''以 bundle.josn 路径来初始化的对象，包含关于该 bundle.josn 的一些属性和操作。 
    @var:  
      - ``__all_errors`` : 表示该文件的所有错误列表。
      - ``__json`` : 表示将该 josn 文件转为 dict 类型后的内容。
      - ``__lines`` : 表示将该 josn 文件转为 list 类型后的内容。  
 
    @method:  
      - ``component_name()`` : 返回该 bundle.json 所在部件名。
      - ``subsystem_name()`` : 返回该 bundle.json 所在子系统名。
      - ``readlines()`` : 返回该 bundle.json 以每一行内容为元素的 list。
      - ``get_line_number(s)`` : 返回 s 字符串在该 bundle.josn 中的行号，未知则返回 0。
      - ``check()`` : 静态检查该 bundle.json，返回错误告警 list。
    '''
    def __init__(self, path:str) -> None:
        self.__all_errors = [] # 该文件的所有错误列表
        self.__json  = {} # 将该 josn 文件转为字典类型内容
        self.__lines = [] # 将该 josn 文件转为列表类型内容
        with open(path, 'r') as f:
            try:
                self.__json = json.load(f)
            except json.decoder.JSONDecodeError as e:
                raise ValueError("'" + path + "'" + " is not a json file.")
        with open(path, 'r') as f:
            self.__lines = f.readlines()

    @property
    def component_name(self) -> str:
        return self.__json["component"]["name"]
    @property
    def subsystem_name(self) -> str: # 目前存在为空的情况
        return self.__json["component"]["subsystem"]
    
    def readlines(self) -> list:
        return self.__lines
    
    def get_line_number(self, s) -> int:
        '''
        @func: 获取指定字符串所在行号。
        '''
        n = 0
        for line in self.__lines:
            n += 1
            if s in line:
                return n
        return 0

    def check(self) -> list:
        '''
        @func: 检查该 bundle.json 规范。
        '''
        n = self.check_name()
        v = self.check_version()
        s = self.check_segment()
        c = self.check_component()
        if n:
            self.__all_errors.append(n)
        if v:
            self.__all_errors.append(v)
        if s:
            self.__all_errors.extend(s)
        if c:
            self.__all_errors.extend(c)

        return self.__all_errors

    # name
    def check_name(self) -> dict:
        bundle_error = dict(line=0, contents='"name"')

        if 'name' not in self.__json:
            bundle_error["description"] = NAME_NO_FIELD
            return bundle_error
        
        name = self.__json['name']
        bundle_error["line"] = self.get_line_number('"name"')
        if not name: # 为空
            bundle_error["description"] = NAME_EMPTY
            return bundle_error

        if '@ohos/' != name[:6]:
            bundle_error["description"] = NAME_FORMAT_ERROR
            return bundle_error

        name_len = len(name.split('/')[1])
        if  name_len < 0 or name_len > 63:
            bundle_error["description"] = COMPONENT_NAME_FROMAT
            return bundle_error

        if not name.islower():
            bundle_error["description"] = NAME_LOWCASE
            return bundle_error

        return None

    # version
    def check_version(self) -> dict:
        bundle_error = dict(line=0, contents='version')

        if 'version' not in self.__json:
            bundle_error["description"] = VERSION_NO_FIELD
            return bundle_error

        bundle_error["line"] = self.get_line_number('"version": ')
        if len(self.__json['version']) < 3: # example 3.1
            bundle_error["description"] = VERSION_ERROR
            return bundle_error
        
        return None

    # segment
    def check_segment(self) -> list:
        bundle_error_segment = []
        bundle_error = dict(line=0, contents='"segment"')

        if 'segment' not in self.__json:
            bundle_error["description"] = SEGMENT_NO_FIELD
            bundle_error_segment.append(bundle_error)
            return bundle_error_segment

        bundle_error["line"] = self.get_line_number('"segment":')
        if 'destPath' not in self.__json['segment']:
            bundle_error["description"] = SEGMENT_DESTPATH_NO_FIELD
            bundle_error_segment.append(bundle_error)
            return bundle_error_segment

        path = self.__json['segment']['destPath']
        bundle_error["line"] = self.get_line_number('"destPath":')
        bundle_error["contents"] = '"segment:destPath"'
        if not path:
            bundle_error["description"] = SEGMENT_DESTPATH_EMPTY
            bundle_error_segment.append(bundle_error)
            return bundle_error_segment

        if type(path) != str:
            bundle_error["description"] = SEGMENT_DESTPATH_UNIQUE
            bundle_error_segment.append(bundle_error)
            return bundle_error_segment
        
        if os.path.isabs(path):
            bundle_error["description"] = SEGMENT_DESTPATH_ABS
            bundle_error_segment.append(bundle_error)
            return bundle_error_segment
        
        return bundle_error_segment

    # component
    def check_component(self) -> list:
        bundle_error_component = []

        if 'component' not in self.__json:
            bundle_error = dict(line=0, contents='"component"', description=COMPONENT_NO_FIELD)
            bundle_error_component.append(bundle_error)
            return bundle_error_component
        
        component = self.__json['component']
        component_line = self.get_line_number('"component":')

        # name
        if 'name' not in component:
            bundle_error = dict(line=component_line,
                                contents='"component"',
                                description=COMPONENT_NAME_NO_FIELD)
            bundle_error_component.append(bundle_error)
        else:
            bundle_error = dict(line=component_line+1, # 同名 "name" 暂用 "component" 行号+1
                                contents='"component:name"')
            if not component['name']:
                bundle_error["description"] = COMPONENT_NAME_EMPTY
                bundle_error_component.append(bundle_error)
            elif 'name' in self.__json and '/' in self.__json['name']:
                if component['name'] != self.__json['name'].split('/')[1]:
                    bundle_error["description"] = COMPONENT_NAME_VERACITY
                    bundle_error_component.append(bundle_error)
        
        # subsystem
        if 'subsystem' not in component:
            bundle_error = dict(line=component_line,
                                contents="component",
                                description=COMPONENT_SUBSYSTEM_NO_FIELD)
            bundle_error_component.append(bundle_error)
        else:
            bundle_error = dict(line=self.get_line_number('"subsystem":'),
                                contents='"component:subsystem"')
            if not component['subsystem']:
                bundle_error["description"] = COMPONENT_SUBSYSTEM_EMPTY
                bundle_error_component.append(bundle_error)
            elif not BundleCheckTools.is_all_lower(component['subsystem']):
                bundle_error["description"] = COMPONENT_SUBSYSTEM_LOWCASE
                bundle_error_component.append(bundle_error)

        # syscap 可选且可以为空
        if 'syscap' not in component:
            pass
        elif component['syscap']:
            bundle_error = dict(line=self.get_line_number('"syscap":'),
                                contents='"component:syscap"')
            err = [] # 收集所有告警
            for i in component['syscap']:
                # syscap string empty
                if not i:
                    err.append(COMPONENT_SYSCAP_STRING_EMPTY)
                    continue
                # check every syscap
                words = i.split('.')
                if words[0] != 'SystemCapability':
                    err.append(COMPONENT_SYSCAP_HEADER_ERROR)
                if len(words) < 3: # SystemCapability.Subsystem.Component[.Subcomponent]
                    err.append(COMPONENT_SYSCAP_STRING_FORMAT_ERROR)
                for j in words:
                    if not j[0].isupper():
                        err.append(COMPONENT_SYSCAP_STRING_STYLE)
            errs = list(set(err)) # 去重告警
            if errs:
                bundle_error["description"] = str(errs)
                bundle_error_component.append(bundle_error)
        
        # feature 可选且可以为空
        if 'features' not in component:
            pass
        elif component['features']:
            bundle_error = dict(line=self.get_line_number('"features":'),
                                contents='"component:features"')
            err = []
            for ft in component["features"]:
                if not ft: # syscap string empty
                    err.append(COMPONENT_FEATURES_STRING_EMPTY)
                    continue
                if (not '_' in ft) or (not ft.startswith(component["name"])):
                    err.append(COMPONENT_FEATURES_SEPARATOR_ERROR)
            errs = list(set(err))
            if errs:
                bundle_error["description"] = str(errs)
                bundle_error_component.append(bundle_error)
        
        # adapted_system_type
        if 'adapted_system_type' not in component:
            bundle_error = dict(line=0, contents='"component"', description=COMPONENT_AST_NO_FIELD)
            bundle_error_component.append(bundle_error)
        else:
            bundle_error = dict(line=self.get_line_number('"adapted_system_type":'),
                                contents='"component:adapted_system_type"')
            ast = component["adapted_system_type"]
            if not ast:
                bundle_error["description"] = COMPONENT_AST_EMPTY
                bundle_error_component.append(bundle_error)
            else:
                type_set = tuple(set(ast))
                if len(ast) > 3 or len(type_set) != len(ast):
                    bundle_error["description"] = COMPONENT_AST_NO_REP
                    bundle_error_component.append(bundle_error)
                else:
                    all_type_list = ["mini", "small", "standard"]
                    error_type = [] # 不符合要求的 type
                    for i in ast:
                        if i not in all_type_list:
                            error_type.append(i)
                    if error_type:
                        bundle_error["description"] = COMPONENT_AST_NO_REP
                        bundle_error_component.append(bundle_error)

        # rom
        if 'rom' not in component:
            bundle_error = dict(line=0, contents='"component:rom"', description=COMPONENT_ROM_NO_FIELD)
            bundle_error_component.append(bundle_error)
        elif not component["rom"]:
            bundle_error = dict(line=self.get_line_number('"rom":'),
                                contents='"component:rom"',
                                description=COMPONENT_ROM_EMPTY)
            bundle_error_component.append(bundle_error)
        else:
            bundle_error = dict(line=self.get_line_number('"rom":'),
                                contents='"component:rom"')
            num, unit = BundleCheckTools.split_by_unit(component["rom"])
            if num <= 0:
                bundle_error["description"] = COMPONENT_ROM_SIZE_ERROR # 非数值或小于0
                bundle_error_component.append(bundle_error)
            elif unit:
                unit_list = ["KB", "KByte", "MByte", "MB"]
                if unit not in unit_list:
                    bundle_error["description"] = COMPONENT_ROM_UNIT_ERROR # 单位有误
                    bundle_error_component.append(bundle_error)
            
        # ram
        if 'ram' not in component:
            bundle_error = dict(line=0, contents='"component:ram"', description=COMPONENT_RAM_NO_FIELD)
            bundle_error_component.append(bundle_error)
        elif not component["ram"]:
            bundle_error = dict(line=self.get_line_number('"ram":'),
                                contents='"component:ram"',
                                description=COMPONENT_RAM_EMPTY)
            bundle_error_component.append(bundle_error)
        else:
            bundle_error = dict(line=self.get_line_number('"ram":'),
                                contents='"component:ram"')
            num, unit = BundleCheckTools.split_by_unit(component["ram"])
            if num <= 0:
                bundle_error["description"] = COMPONENT_RAM_SIZE_ERROR # 非数值或小于0
                bundle_error_component.append(bundle_error)
            elif unit:
                unit_list = ["KB", "KByte", "MByte", "MB"]
                if unit not in unit_list:
                    bundle_error["description"] = COMPONENT_RAM_UNIT_ERROR # 单位有误
                    bundle_error_component.append(bundle_error)

        # deps
        if 'deps' not in component:
            bundle_error = dict(line=0, contents='"component:deps"', description=COMPONENT_DEPS_NO_FIELD)
            bundle_error_component.append(bundle_error)
        else:
            pass
        
        return bundle_error_component


if __name__ == '__main__':
    parse_args()
