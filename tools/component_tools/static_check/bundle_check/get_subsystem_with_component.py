#! /usr/bin/python

import argparse
import os
import json

g_subsystem_path_error = []  # subsystem path exist in subsystem_config.json
g_component_path_empty = []  # bundle.json path which cant get component path.
g_component_abs_path = []   # destPath can't be absolute path.


def get_subsystem_components(ohos_path: str):
    subsystem_json_path = os.path.join(
        ohos_path, r"build/subsystem_config.json")
    subsystem_item = {}

    with open(subsystem_json_path, 'rb') as f:
        subsystem_json = json.load(f)
    # get sunsystems
    for i in subsystem_json:
        subsystem_name = subsystem_json[i]["name"]
        subsystem_path = os.path.join(ohos_path, subsystem_json[i]["path"])
        if not os.path.exists(subsystem_path):
            g_subsystem_path_error.append(subsystem_path)
            continue
        cmd = 'find ' + subsystem_path + ' -name bundle.json'
        bundle_json_list = os.popen(cmd).readlines()
        # get components
        component_list = []
        for j in bundle_json_list:
            bundle_path = j.strip()
            with open(bundle_path, 'rb') as bundle_file:
                bundle_json = json.load(bundle_file)
            component_item = {}
            if 'segment' in bundle_json and 'destPath' in bundle_json["segment"]:
                destpath = bundle_json["segment"]["destPath"]
                component_item[bundle_json["component"]["name"]] = destpath
                if os.path.isabs(destpath):
                    g_component_abs_path.append(destpath)
            else:
                component_item[bundle_json["component"]["name"]
                               ] = "Unknow. Please check " + bundle_path
                g_component_path_empty.append(bundle_path)
            component_list.append(component_item)
        subsystem_item[subsystem_name] = component_list
    return subsystem_item


def get_subsystem_components_modified(ohos_root) -> dict:
    ret = dict()

    subsystem_info = get_subsystem_components(ohos_root)
    if subsystem_info is None:
        return None
    for subsystem_k, subsystem_v in subsystem_info.items():
        for component in subsystem_v:
            for k, v in component.items():
                ret.update({v: {'subsystem': subsystem_k, 'component': k}})
    return ret


def export_to_json(subsystem_item: dict,
                   output_path: str,
                   output_name: str = "subsystem_component_path.json"):
    subsystem_item_json = json.dumps(
        subsystem_item, indent=4, separators=(', ', ': '))
    out_path = os.path.abspath(
        os.path.normpath(output_path)) + '/' + output_name
    with open(out_path, 'w') as f:
        f.write(subsystem_item_json)
    print("output path: " + out_path)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("project", help="project root path.", type=str)
    parser.add_argument("-o", "--outpath",
                        help="specify an output path.", type=str)
    args = parser.parse_args()

    ohos_path = os.path.abspath(args.project)
    if not is_project(ohos_path):
        print("'" + ohos_path + "' is not a valid project path.")
        exit(1)

    output_path = r'.'
    if args.outpath:
        output_path = args.outpath

    return ohos_path, output_path


def is_project(path: str) -> bool:
    '''
    @func: 判断是否源码工程。
    @note: 通过是否含有 .repo/manifests 目录粗略判断。
    '''
    p = os.path.normpath(path)
    return os.path.exists(p + '/.repo/manifests')


def print_warning_info():
    '''@func: 打印一些异常信息。
    '''
    def print_list(list):
        for i in list:
            print('\t' + i)

    # print("subsystem path not exist:")
    # print_list(g_subsystem_path_error)

    if g_component_path_empty or g_component_abs_path:
        print("------------ warning info ------------------")

    if g_component_path_empty:
        print("can't find destPath in:")
        print_list(g_component_path_empty)

    if g_component_abs_path:
        print("destPath can't be absolute path:")
        print_list(g_component_abs_path)


if __name__ == '__main__':
    ohos_path, output_path = parse_args()
    info = get_subsystem_components(ohos_path)
    export_to_json(info, output_path)
    print_warning_info()
